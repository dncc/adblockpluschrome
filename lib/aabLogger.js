/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @module aabLogger */

"use strict";

const {port} = require("./messaging");
const ML_SERVER_URL = "https://api.adblock.ai:8081/submit";


async function init()
{
  port.on("ml.dom", dom =>
  {
    console.log(dom); // eslint-disable-line
    fetch(ML_SERVER_URL, {
      method: "POST",
      body: JSON.stringify(dom),
      headers: {"Content-Type": "application/json"}
    })
    .then(response => { console.log(response); }, // eslint-disable-line
          error => { console.log(error); }); // eslint-disable-line
  });
}

init();
